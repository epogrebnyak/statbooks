# Statistics books for the summer 2020

```
My list of stats books of the rest of summer 2020:

ML:
- Scipy Lectures by https://scipy-lectures.org/preface.html#authors
- AAAMLP(2020) by @abhi1thakur
- HON(2019) by @aureliengeron

Econometrics:
- ROS(2020) by @StatModeling, Jennifer Hill and @avehtari

Links, pics and comments at https://bit.ly/38WSyPg
```

There are two new titles published in summer 2020 on data analysis: "AAAMLP" (machine learning) and "ROS" (econometrics). These two books triggered me to remember other similar titles and how to put a reading on data analysis in a perspective. I soon realised I can sort the learning resources along the following lines:

- **Topics covered**: econometrics vs machine learning vs deep learning
- **Release date, edition**: newly released books vs established textbooks
- **Programming language**: Python vs R vs Julia (also other open source and proprietary)
- **Access**: free vs paid
- **Code**: repository with code examples

So far I've linked 5 titles and the reading path among them looks like below. 

For machine learning: 

  - read Scipy Lectures before "AAAMLP"
  - "HON" (2019) goes well with "AAAMLP" (2020)
  - all above is Python
  - if you are interested in Julia "HON" author has a comprehensive notebook tutorial

For econometrics:

  - go through code examples in "ROS" website, code in R
  - (hm.. not a path yet! but I plan to add more books from https://trics.me/textbook/index.html to here).

Please [let me know](https://gitlab.com/epogrebnyak/statbooks/-/issues/1) if this book listing makes sense to you.

### Scipy Lectures

![](https://img.shields.io/badge/Web-Free-brightgreen)
![](https://img.shields.io/badge/Code-Yes-brightgreen)
![](https://img.shields.io/badge/Language-Python-blue)

Great (but underrated) reading that starts with Python bascis and goes into [machine learning tasks](http://scipy-lectures.org/packages/scikit-learn/index.html) with `scikit-learn` in final chapter.


### Approaching (Almost) Any Machine Learning Problem by Abhishek Thakur ("AAAMLP")

![](https://img.shields.io/badge/Kindle-%2410.19-yellow)
![](https://img.shields.io/badge/Language-Python-blue)

![](https://m.media-amazon.com/images/I/41mmATswuAL.jpg)

- Out as Kindle book mid-2020, see it 
  [on amazon](https://www.amazon.com/Approaching-Almost-Machine-Learning-Problem-ebook/dp/B089P13QHT). 
  It is paid content, but inexpensive.
- [Author twitter](https://twitter.com/abhi1thakur)
- [Rahul Dave recommends this book](https://twitter.com/rahuldave/status/1280080859681271816)

Annotation: 

> This book is for people who have some theoretical knowledge of machine learning and deep learning and want to dive into applied machine learning. The book doesn't explain the algorithms but is more oriented towards how and what should you use to solve machine learning and deep learning problems. The book is not for you if you are looking for pure basics. The book is for you if you are looking for guidance on approaching machine learning problems. The book is best enjoyed with a cup of coffee and a laptop/workstation where you can code along.

### Hands-On Machine Learning with Scikit-Learn, Scikit-Learn, Keras and TensorFlow by Aurélien Géron

<https://homl.info/amazon>

[![](https://img.shields.io/badge/Kindle-%2443.20-red)][hon-amazon]
[![](https://img.shields.io/badge/Code-Yes-brightgreen)][hon-code]
![](https://img.shields.io/badge/Language-Python-blue)

[hon-amazon]: https://www.amazon.com/Hands-Machine-Learning-Scikit-Learn-TensorFlow-ebook-dp-B07XGF2G87/dp/B07XGF2G87/
[hon-code]: https://github.com/ageron/handson-ml2


![](https://images-na.ssl-images-amazon.com/images/I/51aqYc1QyrL._SX379_BO1,204,203,200_.jpg)

- [Author on Twitter](https://twitter.com/aureliengeron)
- See [TOC](https://www.oreilly.com/library/view/hands-on-machine-learning/9781491962282/)

From annotation:

> You’ll learn a range of techniques, starting with simple linear regression and progressing to deep neural networks. 

(You really will.)

### Julia for Pythonistas (Colab notebook) by Aurélien Géron

![](https://img.shields.io/badge/Web-Free-brightgreen)
![](https://img.shields.io/badge/Code-Yes-brightgreen)
![](https://img.shields.io/badge/Language-Python-blue)


This is a walk-through on Julia programming language by author of Hands-On Machine Learning (above).

https://colab.research.google.com/github/ageron/julia_notebooks/blob/master/Julia_for_Pythonistas.ipynb


### Regression and other stories ("ROS")

![](https://img.shields.io/badge/Kindle-%2443.20-red)
![](https://img.shields.io/badge/Code-Yes-brightgreen)
![](https://img.shields.io/badge/Language-R-blue)

![](https://m.media-amazon.com/images/I/41AouVd0sRL.jpg)

- Published by CUP mid-2020, ebook sold [on amazon](https://www.amazon.com/Approaching-Almost-Machine-Learning-Problem-ebook/dp/B089P13QHT)
- See [TOC](https://assets.cambridge.org/97811070/23987/toc/9781107023987_toc.pdf)
- Has a [code repo](https://github.com/avehtari/ROS-Examples)
- Authors on Twitter: [Andrew Gelman](https://twitter.com/StatModeling), Jennifer Hill, and [Aki Vehtari](https://twitter.com/avehtari)
- Predecessor book: [Data Analysis Using Regression and Multilevel/Hierarchical Models](http://www.stat.columbia.edu/~gelman/arm/). ROS is the new edition of this book.


## For further review

This section collects items I might add to learning path later. For now please ignore.

Video:

- [SciPy 2020 playlists](https://www.youtube.com/channel/UCkhm72fuzkS9fYGlGpEmj7A)

Books:

- [@avehtari "BDA" course and book](https://github.com/avehtari/BDA_course_Aalto) (has links to simplier bayesian courses)
- [@rlmcelreath "Statistical rethinking" (SR)](http://xcelab.net/rm/statistical-rethinking/) and [video](https://www.youtube.com/watch?v=4WVelCswXo4&list=PLDcUM9US4XdNM4Edgs7weiyIguLSToZRI&index=1)
- [Full text of ISLR-7](http://faculty.marshall.usc.edu/gareth-james/ISL/ISLR%20Seventh%20Printing.pdf)
- [Data Science at the Command Line](https://twitter.com/jeroenhjanssens/status/1280559408020365318) with code

Papers:

- [P Huenermund on Causal Inference in Machine Learning and AI"](https://twitter.com/PHuenermund/status/1258480147407257605)  (with lessons for econometrics)
- [Athley, Imbens. Machine Learning Methods That Economists Should Know About](https://www.annualreviews.org/doi/abs/10.1146/annurev-economics-080217-053433)
- [Varian. Big Data: New Tricks for Econometrics](https://www.aeaweb.org/articles?id=10.1257/jep.28.2.3)

R learning resources (in response to DataCamp disaster):

- [@djnavarro](https://twitter.com/djnavarro/status/1278470778879569920)
- [@tladeras](https://twitter.com/tladeras/status/1278508718385029120)

Posts:

- Anatomy of a plot and visualisation cheatsheets:
  - [ggplot](https://twitter.com/_isabellamb/status/1269815940629184514)
  - [matplotlib](https://github.com/matplotlib/cheatsheets)


## Quotes

Some quotes to mind awake and open, fooun don Twitter, July 2020.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Statistics means never having to say you’re certain.</p>&mdash; Daniela Witten (@daniela_witten) <a href="https://twitter.com/daniela_witten/status/1281577211456188417?ref_src=twsrc%5Etfw">July 10, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">There are a lot more people who know how to move data around than who know what to do with it.</p>&mdash; Probability Fact (@ProbFact) <a href="https://twitter.com/ProbFact/status/1278359305620729857?ref_src=twsrc%5Etfw">July 1, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Everyone has an idea, what sets people apart is execution.</p>&mdash; Shane Parrish (@ShaneAParrish) <a href="https://twitter.com/ShaneAParrish/status/1278145179032195072?ref_src=twsrc%5Etfw">July 1, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">just because you made the mistakes doesn’t mean you learned the lesson</p>&mdash; Rob Rix (@rob_rix) <a href="https://twitter.com/rob_rix/status/1275224570413359104?ref_src=twsrc%5Etfw">June 23, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
